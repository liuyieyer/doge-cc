### 简介
        集中化的配置中心，目前支持属性文件的托管。存储采用了开源的zookeeper
        client集成 只需要在XML配置对应bean即可， 对于已有的配置无任何入侵
####  安装web控制台
        install JDK8
        git clone https://git.oschina.net/liuyieyer/doge-cc.git
        cd doge-cc
        mvn clean install
        cd doge-cc-admin/target
        java -jar 
        -Dzoo=[your zookeeper address(zk1:2181,{zk1:2181,zk2:2181}) . default localhost:2181] 
        -DzooTimeout=[your zookeeper connected timeout. default 3000ms]
        -Dusername=[your username in login .default admin] 
        -Dpassword=[your password in login .default admin]
        -Dport=[your jetty listen port.default port 8080]
        doge-cc-admin-1.0-SNAPSHOT.jar
        //例如 java -jar -Dport=8888 -Dzoo=localhost:2181 -DzooTimeout=3000 -Dusername=root -Dpassword=123456 doge-cc-admin-1.0-SNAPSHOT.jar
        成功启动后内置的jetty容器 浏览器输入localhost:[your listen port]
        填写启动是输入的账户密码登录即可管理配置，第一次使用时，请添加新的应用
        如添加应用，
                应用名称填写:doge-cc-dubbo 应用之间的属性隔离标示
                属性文件填写:dubbo.port=20881, //注意多个属性之间使用英文逗号隔离
                            dubbo.threads=100
        点击submit成功后将跳转到属性详情页面。

####  集成spring应用
        在pom文件中加入如下依赖
           <dependency>
               <groupId>io.doge.cc</groupId>
               <artifactId>doge-cc-support</artifactId>
               <version>1.0-SNAPSHOT</version>
           </dependency>
           <dependency>
                <groupId>org.apache.zookeeper</groupId>
                <artifactId>zookeeper</artifactId>
                <version>3.4.6</version>
           </dependency>    
        在 spring中加入如下bean
           <bean class="io.doge.cc.support.spring.ZookeeperPropertyPlaceholderConfigurer">
                  <!--应用昵称必填，此处和web控制台的应用名对应 value的值对应web控制台填写的应用名称-->
                  <constructor-arg name="appName" value="doge-cc-dubbo"/>
                  <!-- zookeeper的地址 多机集群模式下（locahost:2181,zookeeper2:2181）此处必须使用和web控制台相同的zk-->
                  <constructor-arg name="zkAddress" value="localhost:2181"/>
                  <!--zookeeper 连接超时时间-->
                  <constructor-arg name="timeout" value="3000"/>
              </bean>
        在配置中获取doge-cc上的属性 
        <dubbo:protocol port="${dubbo.port}" threads="${dubbo.threads}"/>
        
        