package io.doge.cc.support.spring;

import io.doge.cc.core.DogeCc;
import io.doge.cc.core.ZooDogeCc;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by number.liu on 2015/10/28
 */
public class ZookeeperPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    private DogeCc dogeCc;

    public ZookeeperPropertyPlaceholderConfigurer(String appName, String zkAddress, int timeout) {
        dogeCc = new ZooDogeCc(zkAddress, appName, timeout);
    }

    @Override
    protected Properties mergeProperties() throws IOException {
        Properties properties = super.mergeProperties();
        properties.putAll(dogeCc.getProperties());
        return properties;
    }


}
