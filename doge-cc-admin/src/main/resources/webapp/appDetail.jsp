<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>欢迎使用doge-cc</title>
</head>
<body align="center">
<div >
    <div>
        <h2>欢迎使用doge-cc配置中心<a href="/home">首页</a></h2>
        <p>
          <a href="/app?m=toAddAttr&app=${app}">录入新属性</a>
        </p>
    </div>
    <div class="app">
        <p>当前应用有如下properties属性文件</p>
        <table border="0" cellpadding="3" align="center" width="60%" cellspacing="1" style="background-color: #b9d8f3;">
        <c:forEach var="item" items="${properties}">
            <tr style="text-align: center; COLOR: #0076C8; BACKGROUND-COLOR: #F4FAFF; font-weight: bold">
                <td>${item.key}</td>
                <td>${item.value}</td>
                <td><a href="/app?m=toUpdateAttr&app=${app}&key=${item.key}">update</a></td>
                <td><a href="/app?m=delete&app=${app}&key=${item.key}">delete</a> </td>
            </tr>
        </c:forEach>
        </table>
    </div>
</div>
</body>
</html>