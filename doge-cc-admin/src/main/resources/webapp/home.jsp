<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vip
  Date: 2015/10/29
  Time: 12:11
  To change this template use File | Settings | File Templates.
--%>


<html>
<c:if test="${apps eq null}">
    <script>
        window.location.href = '/login'
    </script>
</c:if>
<head>
    <title>欢迎使用doge-cc</title>
</head>
<body align="center">
<div >
    <div>
        <h2>欢迎使用doge-cc</h2>
        <p>
            <a href="addApp.jsp">添加新应用</a>
        </p>
    </div>

    <div class="app" align="center">
        <p>当前配置中心有一下应用</p>
        <table border="0" cellpadding="3" cellspacing="1" align="center" width="60%"  style="background-color: #b9d8f3;">
            <c:forEach var="app" items="${apps}">
                <tr style="text-align: center; COLOR: #0076C8; BACKGROUND-COLOR: #F4FAFF; font-weight: bold">
                    <td><a href="/app?m=detail&app=${app}">${app}</a></td>
                    <td><a  href="/app?m=detail&app=${app}">view</a></td>
                </tr>
            </c:forEach>
        </table>


    </div>
</div>
</body>
</html>
