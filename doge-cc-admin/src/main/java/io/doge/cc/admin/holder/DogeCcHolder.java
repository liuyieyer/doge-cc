package io.doge.cc.admin.holder;

import io.doge.cc.core.DogeCc;
import io.doge.cc.core.ZooDogeCc;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by number.liu on 2015/10/29
 */
public class DogeCcHolder {

    private static ConcurrentHashMap<String, DogeCc> cache = new ConcurrentHashMap<>();

    public static DogeCc getDogeCc() {
        return getDogeCc("doge-cc-web");
    }

    public static DogeCc getDogeCc(String app) {
        DogeCc dogeCc = cache.get(app);
        if (dogeCc == null) {
            dogeCc = new ZooDogeCc(getZoo(), app, getZooTimeout());
            cache.putIfAbsent(app, dogeCc);
        }

        return dogeCc;

    }

    private static String getZoo() {
        return System.getProperty("zoo") == null ? "localhost:2181" : System.getProperty("zoo");
    }

    private static int getZooTimeout() {
        return Integer.parseInt(System.getProperty("zooTimeout") == null ? "3000" : System.getProperty("zooTimeout"));
    }

}
