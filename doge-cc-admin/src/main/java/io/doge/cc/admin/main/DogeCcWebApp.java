package io.doge.cc.admin.main;


import io.doge.cc.admin.jetty.JettyAppServer;
import io.doge.cc.admin.web.servlet.AppServlet;
import io.doge.cc.admin.web.servlet.HomeServlet;
import io.doge.cc.admin.web.servlet.LoginServlet;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Slf4jLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Created by number.liu on 2015/10/29
 */
public class DogeCcWebApp {
    final static Logger logger = LoggerFactory.getLogger(DogeCcWebApp.class);

    public static void main(String[] args) throws Exception {
//        System.setProperty("zoo","localhost:2181");
        assertArgs();
        Log.setLog(new Slf4jLog());
        JettyAppServer jettyAppServer = new JettyAppServer(8080, context -> {
//            context.addServlet(DateServlet.class, "/data/");
            context.addServlet(LoginServlet.class, "/login");
            context.addServlet(AppServlet.class,"/app");
            context.addServlet(HomeServlet.class,"/home");

        });
        jettyAppServer.start();
        jettyAppServer.waitForInterrupt();
    }

    private static void assertArgs() {
        String zoo = System.getProperty("zoo");
        if (Objects.isNull(zoo))
            throw new IllegalArgumentException("please using cmd -Dzoo=[your zookeeper server address]");
        String zooTimeout = System.getProperty("zooTimeout");
        if (Objects.isNull(zooTimeout)) {
            System.setProperty("zooTimeout", "3000");
            logger.warn("zooTimeout is null! please using cmd -Dzoo=[your zooTimeout]! using system default  timeout 3000ms");
        }
        String username = System.getProperty("username");
        if(Objects.isNull(username)){
            System.setProperty("username", "admin");
            logger.warn("username is null,please -Dusernmae=[your login username]! using system default  username [admin]");
        }
        String password = System.getProperty("password");
        if(Objects.isNull(password)){
            System.setProperty("password", "admin");
            logger.warn("password is null,please -Dpassword=[your login password]! using system default  password [admin]");
        }
        String port = System.getProperty("port");
        if(Objects.isNull(port)){
            System.setProperty("port", "8080");
            logger.warn("port is null,please -Dport=[your jetty listen port]! using system default port 8080");
        }

    }
}
