package io.doge.cc.admin.jetty;

import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Created by number.liu on 2015/10/29
 */
public interface WebAppContextCallBack {

    void onContext(WebAppContext appContext);
}
