package io.doge.cc.admin.web.servlet;

import io.doge.cc.admin.holder.DogeCcHolder;
import io.doge.cc.core.DogeCc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Created by Administrator on 2015/11/1.
 */
public class AppServlet extends AbstractServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String m = req.getParameter("m");
        if ("detail".intern() == m.intern())
            doDetail(req, resp);
        if ("add".intern() == m.intern())
            doAdd(req, resp);
        if ("toAddAttr".intern() == m.intern())
            doToAddAttr(req, resp);
        if("delete".intern() == m.intern())
            doDelete0(req, resp);
        if("toUpdateAttr".intern() == m.intern())
            toUpdateAttr(req,resp);
        if("updateAttr".intern() == m.intern()){
            updateAttr(req,resp);
        }
    }

    private void updateAttr(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String app = req.getParameter("app");
        DogeCc dogeCc = DogeCcHolder.getDogeCc(app);
        String key = req.getParameter("key");
        String value = req.getParameter("value");
        dogeCc.update(key,value);
        this.doDetail(req,resp);
    }

    private void toUpdateAttr(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String app = req.getParameter("app");
        DogeCc dogeCc = DogeCcHolder.getDogeCc(app);
        String key = req.getParameter("key");
        String value = dogeCc.get(key);
        req.setAttribute("app",app);
        req.setAttribute("key",key);
        req.setAttribute("value",value);
        req.getRequestDispatcher("/updateAttr.jsp").forward(req,resp);
    }

    private void doDelete0(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String app = req.getParameter("app");
        DogeCc dogeCc = DogeCcHolder.getDogeCc(app);
        String key = req.getParameter("key");
        dogeCc.delete(key);
        this.doDetail(req,resp);
    }

    private void doToAddAttr(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("app", req.getParameter("app"));
        req.getRequestDispatcher("/addAttr.jsp").forward(req, resp);
    }

    private void doAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String app = req.getParameter("app");
        DogeCc dogeCc = DogeCcHolder.getDogeCc(app);
        String ps = req.getParameter("ps");
        String[] props = ps.split(",");
        Arrays.asList(props).stream().sorted().map(String::trim).forEach(prop -> {
            String[] p = prop.split("=");
            if (p.length == 1)
                dogeCc.add(p[0], "");
            else
                dogeCc.add(p[0], p[1]);
        });
        this.doDetail(req,resp);
    }

    private void doDetail(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String app = req.getParameter("app");
        Properties properties = DogeCcHolder.getDogeCc().getProperties(app);
        req.setAttribute("app", app);
        req.setAttribute("properties", new TreeMap<>(properties));
        req.getRequestDispatcher("/appDetail.jsp").forward(req, resp);
    }
}
