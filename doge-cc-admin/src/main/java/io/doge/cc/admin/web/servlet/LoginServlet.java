package io.doge.cc.admin.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by number.liu on 2015/10/29
 */
public class LoginServlet extends AbstractServlet {


    public LoginServlet() {
        System.out.println(this.getClass() + " init  success !!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if(username == null || password == null){
            resp.sendRedirect("/");
            return;
        }
        if(username.equals(System.getProperty("username")) && password.equals(System.getProperty("password"))){
            req.getSession().setAttribute("user","user");
            resp.sendRedirect("/home");
        }else{
            resp.sendRedirect("/");
        }



    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
