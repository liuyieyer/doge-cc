package io.doge.cc.admin.web.servlet;

import io.doge.cc.admin.holder.DogeCcHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by 刘毅 on 2015/11/2
 */
public class HomeServlet extends AbstractServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getSession().getAttribute("user")==null){
            resp.sendRedirect("/");
            return;
        }
        req.setAttribute("apps", DogeCcHolder.getDogeCc().getChildren());
        req.getRequestDispatcher("/home.jsp").forward(req,resp);
    }
}
