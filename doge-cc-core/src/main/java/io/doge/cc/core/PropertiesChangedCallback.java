package io.doge.cc.core;

import java.util.Properties;

/**
 * Created by number.liu on 2015/10/28
 */
public interface PropertiesChangedCallback {

    void onChanged(Properties properties);
}
