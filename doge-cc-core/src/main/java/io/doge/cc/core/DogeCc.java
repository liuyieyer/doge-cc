package io.doge.cc.core;

import java.util.List;
import java.util.Properties;

/**
 * Created by number.liu on 2015/10/28
 */
public interface DogeCc {

    Properties getProperties();
    Properties getProperties(String app);
    List<String> getChildren(String parent);

    /**
     * 获取到默认doge-cc下所以的app
     * @return
     */
    List<String> getChildren();


    void onChanged(PropertiesChangedCallback properties);

    void setProperties(Properties properties);

    void add(String key, String value);

    void delete(String key);

    void close();


    String get(String key);

    void update(String key, String value);
}
